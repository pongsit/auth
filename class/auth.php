<?php

namespace pongsit\auth;

class auth{
	
	private $db;
	public $variables=array();
	
	public function __construct() 
    {
	    $this->db = $GLOBALS['db'];
		$this->variables['logout-menu']='';
    }
	function login($username,$password){
		$passwordIn = $password;
		$user = new \pongsit\user\user();
		$user_id = $user->get_id($username);
		if(empty($user_id)){
			return false;
		}
		$user_infos = $user->get_info($user_id);
		if(!$user_infos['active']){
			if($user_id!=1){
				return false;
			}
		}

		if($this->password_verify($passwordIn, $user_infos['password'])){
			$this->add_session(array('user','id'),$user_infos['id']);
			$this->add_session(array('user','username'),$user_infos['name']);
			$this->add_session(array('user','name'),$user_infos['name']);
			$this->add_session(array('user','baseurl'),$GLOBALS['baseurl']);
			
			return true;	
		}else{
			return false;
		}
	}
	function logout(){
// 		print_r($_COOKIE);
// 		exit();
		foreach($_COOKIE as $k=>$v){
			if(is_array($v)){
				foreach($v as $_k=>$_v){
					setcookie($k.'['.$_k.']', $_v, time()-3600, '/');
				}
			}else{
				setcookie($k, $v, time()-3600, '/');
			}
		}
		unset($_COOKIE);
		session_unset();
		return true;
	}
	function generatePassword($password){
		return $this->password_hash($password, PASSWORD_DEFAULT, ['cost' => 4]);
	}
	function lock($enable = false){
		if($enable == false){ return; }
		if(!empty($_COOKIE['user'])){
			$_SESSION['user'] = $_COOKIE['user'];
		}
		if(empty($_SESSION['access_token'])){
			if(empty($_SESSION['user'])){
				$url = new \pongsit\url\url();
				$current_page = $url->get_current_basename();
				$skips = $GLOBALS['confVariables']['auth_skip'];
				$skips[] = 'index.php';
				$skips[] = 'login.php';
				$skips[] = 'logout.php';
				$skips[] = 'setting.php';
				$skips[] = 'install.php';
				$skips[] = 'signup.php';
				$skips[] = 'policy.php';
				if(!in_array($current_page,$skips)){
					header('Location: '.$GLOBALS['path_to_core'].'auth/login.php');
				}
			}else{
				if($GLOBALS['baseurl'] != $_SESSION['user']['baseurl']){
					$this->logout();
					header('Location: '.$GLOBALS['path_to_core'].'auth/login.php');	
				}
			}
		}
	}
	function lock_for($roles){
		$role = new \pongsit\role\role();
		$pass = 0;
		foreach($role->show() as $role_user){
			if(in_array($role_user,$roles)){
				$pass = 1;
			}
		}
		if($pass == 0){
			$view = new \pongsit\view\view('message');
			$variables = array();
			$variables['message'] = 'คุณไม่มีสิทธิ์ใช้หน้านี้ครับ';
			echo $view->create($variables);
			exit();
		}
	}
	function password_hash($password, $algo=PASSWORD_DEFAULT, $options=array()){
        $crypt = NEW \pongsit\password\password();
        $crypt->setAlgorithm($algo);
        
        $debug  = isset($options['debug'])
                ? $options['debug']
                : NULL;
        
        $password = $crypt->generateCryptPassword($password, $options, $debug);
        
        return $password;
    }
    function password_verify($password, $hash){
        return (crypt($password, $hash) === $hash);
    }
    function password_needs_rehash($hash, $algo, $options=array()){
        $crypt = NEW password();
        return !$crypt->verifyCryptSetting($hash, $algo, $options);
    }
    
    function add_session($names,$value){
    	// allow maximum 2 level array
    	if(is_array($names)){
    		$count = count($names);
    		switch($count){
    			case 1:
    				$_SESSION[$names[0]] = $value;
    				setcookie($names[0], $value, time() + (10 * 365 * 24 * 60 * 60), '/');
    				break;
    			case 2: 
    				$_SESSION[$names[0]][$names[1]] = $value;
    				setcookie($names[0].'['.$names[1].']', $value, time() + (10 * 365 * 24 * 60 * 60), '/');
    				break;
    		}
    	}else{
    		$_SESSION[$names] = $value;
			setcookie($names, $value, time() + (10 * 365 * 24 * 60 * 60), '/');
    	}
    }
    function safe_encrypt(string $data, string $key){
		$method = 'AES-256-CBC';
		$ivSize = openssl_cipher_iv_length($method);
		$iv = openssl_random_pseudo_bytes($ivSize);

		$encrypted = openssl_encrypt($data, $method, $key, OPENSSL_RAW_DATA, $iv);
	
		// For storage/transmission, we simply concatenate the IV and cipher text
		$encrypted = base64_encode($iv . $encrypted);

		return $encrypted;
	}
	function safe_decrypt(string $data, string $key){
		$method = 'AES-256-CBC';
		$data = base64_decode($data);
		$ivSize = openssl_cipher_iv_length($method);
		$iv = substr($data, 0, $ivSize);
		$data = openssl_decrypt(substr($data, $ivSize), $method, $key, OPENSSL_RAW_DATA, $iv);

		return $data;
	}
    function is_logged_in(){
    	if(!empty($_SESSION['user']['id'])){
    		return true;
    	}else{
    		return false;
    	}
    }
}