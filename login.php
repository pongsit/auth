<?php
	
require_once '../system/init.php'; 

$user = new \pongsit\user\user();
// $invitation = new Invitation();

$notification = '';
if(!empty($_POST['username']) && !empty($_POST['password'])){
	$login = $auth->login($_POST['username'],$_POST['password']);
	if(!$login){
		$notification = $view->block('alert',array('type'=>'danger','css'=>'col-sm-10 col-md-8 col-lg-6','message'=>'Username หรือ Password ไม่ถูกต้อง'));
	}else{
		$redirect = $path_to_root.'index.php';
		header('Location: '.$redirect);
		exit();
	}
}

// $invitation_infos = $invitation->get_data();

// add view
$variables = array();
$variables['h1'] = $view->block('h1',array('message'=>'เข้าสู่ระบบ','css'=>'col-sm-10 col-md-8 col-lg-6 col-xl-4 text-center'));
$variables['hide-main-menu'] = 'hide';
$variables['notification'] = $notification;
// เปลี่ยน header เฉพาะหน้านี้
// $variables['header'] = $view->block('header');
$variables['sign_up_hide']='hide';
// if($invitation_infos['active']==1){
// 	$variables['sign_up_hide']='';
// }

$variables['social-login'] = $view->block('social-login');

echo $view->create($variables);
