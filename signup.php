<?php
	
require_once '../system/init.php'; 

$variables['h1'] = $view->block('h1',array('message'=>'สมัครสมาชิก','css'=>'col-sm-10 col-md-8 col-lg-6 col-xl-4 text-center'));
$variables['hide-main-menu'] = 'hide';
// เปลี่ยน header เฉพาะหน้านี้
// $variables['header'] = $view->block('header');
$variables['sign_up_hide']='hide';
// if($invitation_infos['active']==1){
// 	$variables['sign_up_hide']='';
// }

$variables['social-login'] = $view->block('social-login');

echo $view->create($variables);
